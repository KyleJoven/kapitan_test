<?php
// Routes


//========================================
// CORS
//========================================
if (isset($_SERVER['HTTP_ORIGIN'])) {
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
    header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

  exit(0);
}

        //Load Messages
    $app->get('/loadMsg', function ($request, $response) {
        $query = "Select * from tbl_messages";
        $stmt = $this->db->prepare($query);
        $res = $stmt->execute();
        $result = $stmt->fetchAll();
        echo json_encode($result);
    });

        //Insert Message
    $app->post('/insertMsg', function ($request, $response) {
        $data = $request->getParsedBody();
        $msg = filter_var($data['message'], FILTER_SANITIZE_STRING);
        $query = "Insert into tbl_messages (message) values (:msg)";
        $stmt = $this->db->prepare($query);
        $stmt->execute(["msg" => $msg]);
        $response = 'success';
        return $response;
    });


        //Delete Message
    $app->post('/deleteMsg', function ($request, $response) {
        $data = $request->getParsedBody();
        $id = filter_var($data['id'], FILTER_SANITIZE_STRING);
        $query = "Delete from tbl_messages where id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->execute(["id" => $id]);
        $response = 'success';
        return $response;
    });


        //Edit Message
    $app->post('/editMsg', function ($request, $response) {
        $data = $request->getParsedBody();
        $id = filter_var($data['id'], FILTER_SANITIZE_STRING);
        $msg = filter_var($data['message'], FILTER_SANITIZE_STRING);
        $query = "Update tbl_messages set message = :msg where id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->execute(["id" => $id,"msg" => $msg]);
        $response = 'success';
        return $response;
    });


        //Search Message
    $app->post('/searchMsg', function ($request, $response) {
        $data = $request->getParsedBody();
        $msg = filter_var($data['message'], FILTER_SANITIZE_STRING);
        $query = "Select * from tbl_messages where message like '%".$msg."%'";
        $stmt = $this->db->prepare($query);
        $stmt->execute(["message" => $msg]);
        $result = $stmt->fetchAll();
        echo json_encode($result);
    });