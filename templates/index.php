<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Sample</title>
         <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
        <style>
            table {
                margin: 0px 0px 0px 100px;
                padding: 0;
                width: 50%;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                text-align: center;
                color: #000000;
                font-size: 18px;
                border-collapse: collapse;
                overflow-y: scroll;
            }

            table, th, td {
                border-bottom: 1px solid #ddd;
            }
            th {
                height: 50px;
            }
            tr:hover {
                background-color: #f5f5f5;
            }
            button {
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                background-color: #0aa;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            }
            button:hover {
                background-color: #aaa;
            }
            .post {
                margin-left: 15px;
            }
            input[type=text] {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                width: 40%;
                padding: 12px 20px;
                margin: 10px 0 0 100px;
                box-sizing: border-box;
                border: 3px solid #ccc;
                -webkit-transition: 0.5s;
                transition: 0.5s;
                outline: none;
                font-size: 16px;
            }
            
            #search {
                width: 50%;
                padding: 12px 20px;
                margin: 10px 0 0 100px;
                font-size: 16px;
            }
            
            input[type=text]:focus {
                border: 3px solid #555;
            }
            
        </style>
    </head>
    <body>
        <input type="text" id="search" placeholder="Search">
            <div style="overflow-x:auto;">
                <table id="tableMsg">
                    <tr>
                        <th>
                        Messages
                        </th>
                        <th>
                        Action
                        </th>
                    </tr>
                </table>

                </div>
            <input type="text" id="update" placeholder="Type here...">
            <button class="post" value="">Submit</button></br></br>
        <script>
            
            $(document).ready(function(){
            genTbl();
            
            //Generate Table
            function genTbl(){
                    $.ajax({
                    url: "http://localhost/kapitan_test/public/loadMsg",
                    data: 'json',
                    success: function(data) {
                        var tbl;
                        var temp = $.parseJSON(data);
                        temp.forEach(function (i) { 
                        tbl += '<tr id="tblRow"><td class="msg" value="'+ i.id +'">' + i.message + '</td><td> <button value="'+ i.id +'" class="editt" type="button"> Edit </button> <button value="'+ i.id +'" class="deletee" type="button"> Delete </button></td></tr>';
                        });
                        $('#update').val("");
                        $("#tableMsg").find("tr:gt(0)").remove();
                        $('#tableMsg').append(tbl);
                        $(".post").html('Submit');
                    }
                });
            }; 
            
                
            //Search Table
           $( "#search" ).keyup(function(e) {
                    var temp = $("#search").val();      //Message typed on search bar
                    $.ajax({
                    type: "POST",
                    url: "http://localhost/kapitan_test/public/searchMsg",
                    data: "message="+temp,
                    success: function(data) {
                        var tbl;
                        var temp = $.parseJSON(data);
                        temp.forEach(function (i) {
                        tbl += '<tr id="tblRow"><td id="msg" value="'+ i.id +'">' + i.message + '</td><td> <button value="'+ i.id +'" class="editt" type="button"> Edit </button> <button value="'+ i.id +'" class="deletee" type="button"> Delete </button></td></tr>';
                        });
                        $('#update').val("");
                        $("#tableMsg").find("tr:gt(0)").remove(); //Removes all rows except the first one
                        $('#tableMsg').append(tbl);
                    }
                });
            });    
            
            //Insert Message
            $( ".post" ).click(function(e) {
                var temp = $("#update").val();          //Message value
                var temp2 = $(this).val();              //Message ID
                if(temp != ""){                         
                if($(".post").text() == "Submit"){      //Inserts Message
                    $.ajax({
                    type: "POST",
                    url: "http://localhost/kapitan_test/public/insertMsg",
                    data: "message="+temp,
                    success: function(data) {
                        genTbl();
                    }
                });
                }
                else if($(".post").text() == "Edit"){   //Updates Message
                        $.ajax({
                        type: "POST",
                        url: "http://localhost/kapitan_test/public/editMsg",
                        data: "id="+temp2+"&message="+temp,
                        success: function(data) {
                            genTbl();
                        }
                    });
                }
                }
                else{
                alert("Empty Message");
                }
                e.preventDefault();
            });
            
            
             //Delete Message
            $("#tableMsg").on('click', '.deletee', function(e) {
                if(confirm("Delete this message?")){
                    var temp = $(this).val();           //Message ID
                    $.ajax({
                    type: "POST",
                    url: "http://localhost/kapitan_test/public/deleteMsg",
                    data: "id="+temp,
                    success: function(data) {
                        genTbl();
                    }
                });
                }
                e.preventDefault();
            });
                
                
             //Get Message
            $( "#tableMsg").on('click', '.editt', function(e) {
                var temp = $(this).val();                               //Message ID
                $("#update").val($(this).closest('td').prev().text());  //Puts the value to textbox
                $(".post").val(temp);
                $(".post").html('Edit');
                e.preventDefault();
            });
         });
        </script>
    </body>
</html>
